#!/bin/sh
export __SERVER_IP='http://localhost:9191'

# RUN nginx in MAC
# sudo nginx  -p "$(pwd)" -c conf/nginx.conf
# sudo nginx  -p "$(pwd)" -c conf/nginx.conf  -sstop
# RUN nginx in Windows
# nginx

function __escape_path(){
  echo $1 | sed 's/ /\\ /g'
  return 0;
}

function .setup(){
  export SCRIPT_PATH=$(echo "${BASH_SOURCE[0]}");
  export BASE_FOLDER="$(cd $(dirname "$SCRIPT_PATH"); pwd )";
  export NGINX_HOME="$BASE_FOLDER/nginx_local";

  echo "PATHS";
  echo "================";
  echo "NGINX_HOME     :  $NGINX_HOME";
  # echo "$SCRIPT_PATH";
  echo "BASE_FOLDER  :  $BASE_FOLDER";
  echo "================";
}

function .install(){
  local __HTTP_PROXY=$HTTP_PROXY
  local __HTTPS_PROXY=$HTTPS_PROXY
  unset HTTP_PROXY
  unset HTTPS_PROXY
  ## $(cd $(dirname "$(__escape_path "${BASH_SOURCE[0]}")"), pwd)
  local __temp="";
  local cwd=$(pwd);
  local html_folder=$NGINX_HOME/html;
  local html_downloads_folder=$html_folder/downloads;
  local html_conf_folder=$NGINX_HOME/conf;
  local html_libs_folder=$html_folder/libs;

  echo -e "Source folder: $SCRIPT_FOLDER"
  echo -e "Target folder: $NGINX_HOME"

  rm -rf __tmp
  rm -rf $NGINX_HOME
  mkdir $BASE_FOLDER/__tmp/

  curl -s $__SERVER_IP/downloads/nginx-1.7.11.zip -o __tmp/nginx.zip
  curl -s $__SERVER_IP/downloads/bootstrap-3.3.4-dist.zip -o __tmp/bootstrap.zip
  curl -s $__SERVER_IP/downloads/dojo-release-1.10.4.zip -o __tmp/dojo.zip
  curl -s $__SERVER_IP/downloads/jquery.tgz -o __tmp/jquery.tgz
  curl -s $__SERVER_IP/downloads/nginx_html_student.tgz -o __tmp/nginx_html_student.tgz
  curl -s $__SERVER_IP/downloads/nginx-student.conf -o __tmp/nginx-student.conf

  unzip -q __tmp/nginx.zip
  
  mv -f nginx-* $NGINX_HOME

  mkdir -p $html_folder/images
  mkdir -p $html_libs_folder

  cp -f "__tmp/nginx-student.conf" "$html_conf_folder/nginx.conf"

  
  echo "expanding bootstrap"
  $(cd "$html_libs_folder" ; unzip -q "$BASE_FOLDER/__tmp/bootstrap.zip" ; mv bootstrap-* bootstrap);
  echo "expanding dojotoolkit"
  $(cd "$html_libs_folder"; unzip -q "$BASE_FOLDER/__tmp/dojo.zip"; mv dojo-release-* dojoroot);
  echo "expanding jquery"
  $(cd "$html_libs_folder"; tar -xf "$BASE_FOLDER/__tmp/jquery.tgz");
  echo "expanding html files"
  $(cd "$NGINX_HOME"; tar -xf "$BASE_FOLDER/__tmp/nginx_html_student.tgz");

  ## restore proxies
  export HTTP_PROXY=$__HTTP_PROXY
  export HTTPS_PROXY=$__HTTPS_PROXY
  ## rm -rf $BASE_FOLDER/__tmp/
}
