var dojoConfig = {
      baseUrl: '/libs/dojoroot/',
      locale:'en',
      parseOnLoad:true,
      async:true,
      //here are the packages dojo will be aware of and related js files
      packages: [
          { name: 'dojo', location: 'dojo' },
          { name: 'dijit', location: 'dijit' },
          { name: 'dojox', location: 'dojox' },
          { name: 'webapp', location: '/chapters/2_dojotoolkit/webapp' }          
        ],
      app:{
        baseUrl:'/',
        serviceUrl:'#{serviceUrl}',
        excelUrl:'#{excelUrl}',
      }
    };