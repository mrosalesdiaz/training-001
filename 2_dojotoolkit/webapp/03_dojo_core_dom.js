define( [
  'dojo/_base/lang',
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dojo/dom-class',
  'module'
], function( lang, domConstruct, domAttr, domClass, module ) {
  var NEW_DOM_TEMPLATE = '&nbsp;<span class="glyphicon glyphicon-th-large" aria-hidden="true"></span><sup>{index}</sup>&nbsp;';
  var DEFAULT_BTN_TEMPLATE = '<button id="btn_object" class="btn btn-success btn-block">Place</button>';
  var index = 0;
  module.exports.placeBefore = function() {
    // SECTION#1: DOM Place before 
    // SECTION#1
  };
  module.exports.placeAfter = function() {
    // SECTION#2: DOM Place after
    // SECTION#2
  };
  module.exports.placeReplace = function() {
    // SECTION#3: DOM Place replace
    // SECTION#3
  };
  module.exports.placeOnly = function() {
    // SECTION#4: DOM Place only
    // SECTION#4
  };
  module.exports.placeFirst = function() {
    // SECTION#5: DOM Place first
    // SECTION#5
  };
  module.exports.placeLast = function() {
    // SECTION#6: DOM Place last
    // SECTION#6
  };
  module.exports.empty = function() {
    // SECTION#7: DOM Place empty
    // SECTION#7
  };
  module.exports.reset = function() {
    // SECTION#8: Reset example
    // SECTION#8
  };
  // dim attr
  var __genericFunction = function( direction ) {
    event.preventDefault()
    // SECTION#9: Generic function to handle pager clicks, move next, previous and show current page 
    var pagerDOM = document.getElementById( "ul_pager" );
    var pagerInfo = document.getElementById( "pages_info" );
    var pagerData = JSON.parse( domAttr.get( pagerDOM, 'data-pager' ) );

    if ( direction == 'previous' && pagerData.currentPage - 1 >= 0 ) {
      pagerData.currentPage--;
    } else if ( direction == 'next' && pagerData.currentPage + 1 < pagerData.totalPages ) {
      pagerData.currentPage++;
    }
    pagerData.currentPageLabel = pagerData.currentPage + 1;
    domConstruct.place( lang.replace( '<li id="pages_info">Page: {currentPageLabel} / {totalPages}</li>', pagerData ), pagerInfo, 'only' )
    domAttr.set( pagerDOM, 'data-pager', JSON.stringify( pagerData ) );

    var domNodePrevious = document.getElementById( "b_previous" );
    var domNodeNext = document.getElementById( "b_next" );

    if ( pagerData.currentPage <= 0) {
      domClass.add( domNodePrevious, 'disabled' );
    }else if(domClass.contains(domNodePrevious,'disabled')){      
      domClass.remove( domNodePrevious, 'disabled' );      
    }

    if ( pagerData.currentPage >= pagerData.totalPages-1) {
      domClass.add( domNodeNext, 'disabled' );
    }else if(domClass.contains(domNodeNext,'disabled')){      
      domClass.remove( domNodeNext, 'disabled' );      
    }
    // SECTION#9
  };
    // SECTION#10: Change to lang partial
  module.exports.pagePrevious = function () {
    
  };
  module.exports.pageNext = function () {
    
  }
  // SECTION#10
  // dom class
  module.exports.toggleError = function() {
    // SECTION#11: toggle error class
    // SECTION#11
  }
  module.exports.successError = function() {
    // SECTION#12: Replace has-error and dummy-class by success and summy-success-class
    // SECTION#12
  }
  module.exports.resetClass = function() {
    // SECTION#13: back to default value
    // SECTION#13
  }
  module.exports.showError = function() {
    // SECTION#14: add has-error
    // SECTION#14
  }
  module.exports.removeError = function() {
    // SECTION#15: remove has error
    // SECTION#15
  }
} );