define( [
  'dojo/request'
  , 'dojo/_base/lang'
  , 'dojo/dom-construct'
  , 'dojo/query'
  , 'dojo/io-query'
  , 'module'
  // no paramter
  , 'dojo/NodeList-dom'
  ], function( request, lang, domConstruct, query, ioQuery, module ) {
  var successHandler = function( results ) {
    var tableTbody = query( '#t_results > tbody' );
    var ROW_TEMPLATE = "<tr><td>{id}</td><td>{properties.title}</td><td>{properties.type}</td><td>{properties.place}</td><td>{properties.mag}</td></tr>";
    tableTbody.empty();
    for ( var i = 0; i < results.features.length; i++ ) {
      var item = results.features[ i ]
      var trDom = domConstruct.toDom( lang.replace( ROW_TEMPLATE, item ) );
      tableTbody.addContent( trDom );
    };
    console.log( "done:\n" + Date.now() )
  }
  var errorHandler = function() {
    console.log( arguments );
  }
  var progressHandler = function( result ) {    
    
    console.log( "progress:\n" + result.loaded +" / " )
  }
  module.exports.loadTable = function() {
    var options = {
      headers: {
        'X-Requested-With':null,
      },
      handleAs: 'json'
    }
    //var url = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_month.geojson"

    var url = "http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2015-01-01&endtime=2015-02-02"

    request( url, options ).then( successHandler, errorHandler, progressHandler );
  }
} );