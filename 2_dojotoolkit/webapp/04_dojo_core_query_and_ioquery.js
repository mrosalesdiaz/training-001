define( [
  'dojo/query',
  'dojo/io-query',
  'dojo/dom-class',
  'module',
  //
  'dojo/NodeList-dom',
  'dojo/NodeList-html'
   ], function( query, ioQuery, domClass, module ) {
  module.exports.setQuery = function( selector ) {
    query( '#it_query' ).attr( 'value', selector );
    $( '#query-examples' ).collapse( 'hide' );
  };
  module.exports.processQuery = function() {
    // SECTION#1: Get query and class then apply
    // SECTION#1    
  };
  module.exports.encode = function() {
    // SECTION#2: encode data
    // especial parser for JSON
    // data = new Function('return '+data).apply(null);
    // SECTION#2
  };
  module.exports.decode = function() {
    // SECTION#3: decode data
    // SECTION#3
  };
} );