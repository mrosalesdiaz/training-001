// SECTION#1: Declare module
define( [ 'dojo/_base/lang' ], function( lang ) {
  console.log( "module loaded." )
  var album = {
    published_date: new Date( 2012, 6, 30 ), // July 30, 2012
    number_of_disks: 1,
    title: "Sin Bandera",
    reviews: {
      number: 2,
      data: [ {
        user: "cnunez",
        text: "delicious songs"
        }, {
        user: "cnunez",
        text: "wonderful album"
        } ]
    }
  };

  function test_dojo_exists() {
    console.log( '##################################################' );
    console.log( '## Dojo Exists' );
    console.log( '#####' );
    console.log( 'DATA:' );
    console.log( JSON.stringify( album ) );
    console.log( '#####' );
    console.log( '' );
    console.log( 'Does exist "title" in object album?' );
    // SECTION#2: Implement exists title
    // SECTION#2
    console.log( 'Does exist "reviews.number" in object album?' );
    // SECTION#2: Implement exists reviews.number
    // SECTION#2
    console.log( 'Does exist "reviews.date" in object album?' );
    // SECTION#2: Implement exists reviews.date
    // SECTION#2
    console.log( 'Does exist "reviews.data.0" in object album?' );
    // SECTION#2: Implement exists reviews.data.0
    // SECTION#2
    console.log( 'Does exist "reviews.data.3" in object album?' );
    // SECTION#2: Implement exists reviews.data.3
    // SECTION#2
    console.log( '' );
    console.log( '##################################################' );
  }

  function test_dojo_mixin() {
    var songs = {
      songs: [
        {
          number: "1",
          title: "Para Alcanzarte"
        },
        {
          number: "2",
          title: "Kilometros"
        },
        {
          number: "3",
          title: "A Encontrarte (Dueto Con Aureo Baqueiro)"
        },
        {
          number: "4",
          title: "A Primera Vista"
        },
        {
          number: "5",
          title: "Entra En Mi Vida"
        },
        {
          number: "6",
          title: "Te Vi Venir"
        },
        {
          number: "7",
          title: "Sirena"
        },
        {
          number: "8",
          title: "No Neguemos El Amor"
        },
        {
          number: "9",
          title: "Si Me Besas"
        },
        {
          number: "10",
          title: "Ves"
        },
        {
          number: "11",
          title: "Entra En Mi Vida (Version Ranchera)"
        },
        {
          number: "12",
          title: "Entrade En Mi Vida (Version Grupera)"
        }

      ]
    };
    console.log( '##################################################' );
    console.log( '## Dojo Mixin' );
    console.log( '#####' );
    console.log( 'DATA:' );
    console.log( JSON.stringify( album ) );
    console.log( '#####' );
    console.log( '' );
    console.log( 'Mix with songs' );
    // SECTION#3: Mixin songs
    // SECTION#3
    console.log( 'Mix with review source' );
    // SECTION#3: Mixin {reviews:{source:"Amazon"}}
    // SECTION#3
    console.log( 'Result after mixin' );
    console.log( JSON.stringify( album ) );
    console.log( '' );
    console.log( '##################################################' );
  }

  function test_dojo_hitch() {
    var fnHitched = function() {
      console.log( 'Found attributes in scope "this":' );
      console.log( this );
      return lang.exists( 'title', this );
    };
    console.log( '##################################################' );
    console.log( '## Dojo Hitch' );
    console.log( '#####' );
    console.log( 'DATA:' );
    console.log( JSON.stringify( album ) );
    console.log( '#####' );
    console.log( '' );
    console.log( 'Does "this.title" exists?' );
    // SECTION#4: hitch the function    
    // SECTION#4
    console.log( '' );
    console.log( '##################################################' );
  }

  function test_dojo_partial() {
    var fnPartial = function( externalParam1, externalParam2, param ) {
      if ( arguments.length == 1 ) {
        param = externalParam1;
        externalParam1 = externalParam2 = undefined;
      }
      console.log( "externalParam1: " + externalParam1 );
      console.log( "externalParam2: " + externalParam2 );
      console.log( "param: " + param );
    }
    console.log( '##################################################' );
    console.log( '## Dojo Partial' );
    console.log( '#####' );
    console.log( 'DATA:' );
    console.log( JSON.stringify( album ) );
    console.log( '#####' );
    console.log( '' );
    fnPartial( "Internal Value" );
    console.log( 'After partial' );
    // SECTION#5: Implement partial and test
    // SECTION#5
    console.log( '' );
    console.log( '##################################################' );
  }

  function test_dojo_replace() {
    console.log( '##################################################' );
    console.log( '## Dojo Replace' );
    console.log( '#####' );
    console.log( 'DATA:' );
    console.log( JSON.stringify( album ) );
    console.log( '#####' );
    console.log( '' );
    console.log( '## Basic Replace' );
    //SECTION#6: Basic Replace replace album format [ {published_date} ] {title} Dics: {number_of_disks}
    //SECTION#6
    console.log( '## Arrays ' );
    //SECTION#7: Arrays replace show first and second song
    //SECTION#7
    //SECTION#8: Custom functions, show year from published_date. "{title} ({published_date:y})"
    //SECTION#8
    console.log( '' );
    console.log( '##################################################' );
  }

  function test_dojo_trim() {
    console.log( '##################################################' );
    console.log( '## Dojo Trim' );
    console.log( '#####' );
    console.log( 'DATA:' );
    console.log( JSON.stringify( album ) );
    console.log( '#####' );
    console.log( '' );    
    var str0 = '   hello world ';
    var str1 = '  ';
    // SECTION#9: call trim 
    // SECTION#9
    console.log( '' );
    console.log( '##################################################' );
  }
  test_dojo_exists();
  test_dojo_mixin();
  test_dojo_hitch();
  test_dojo_partial();
  test_dojo_replace();
  test_dojo_trim();
} );
// SECTION#1